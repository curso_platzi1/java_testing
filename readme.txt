Comenzamos el curso de JavaTesting en Platzi, donde nos adentraremos en el mundo del Desarrollo Guiado por Pruebas (TDD). 
Durante este curso, exploraremos el uso de poderosas herramientas y bibliotecas como JUnit 4 y Mockito para llevar a cabo pruebas exhaustivas en nuestro código.

El objetivo principal de este curso es aprender a desarrollar software siguiendo las mejores prácticas de TDD. 
Esto significa que, antes de escribir el código de producción, primero escribimos pruebas que describen el comportamiento deseado. 
Luego, iteramos sobre el desarrollo y las pruebas para garantizar que nuestro software funcione de la manera esperada.

En el transcurso del curso, aplicamos JUnit 4 para escribir y ejecutar pruebas unitarias en clases y métodos. 
JUnit 4 es una poderosa biblioteca de pruebas que ayuda a validar el comportamiento de aplicaciones de manera automatizada.

Además, utilizamos Mockito, otra biblioteca fundamental en el desarrollo de pruebas, para crear simulaciones (mocks) de objetos y controlar su comportamiento en pruebas. 
Esto permitirá aislar las unidades de código que probamos y garantizar la integridad de nuestras pruebas.
