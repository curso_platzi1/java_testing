package player;

import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;

/**
 *
 * @author Avyla
 */
public class PlayerTest {
    
    @Test
    public void loses_when_dice_number_is_too_low() {
        Dice dice = Mockito.mock(Dice.class);
        
        Mockito.when(dice.roll()).thenReturn(2);
        
        Player player = new Player(dice,3);
        assertFalse(player.play());
    }
    
    @Test
    public void loses_when_dice_number_is_too_big() {
        Dice dice = Mockito.mock(Dice.class);
        
        Mockito.when(dice.roll()).thenReturn(6);
        
        Player player = new Player(dice,3);
        assertTrue(player.play());
        
    }
    
}
