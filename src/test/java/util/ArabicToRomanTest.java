package util;

import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;
import static org.junit.Assert.*;

public class ArabicToRomanTest {
    
    @Test
    public void arabic_to_roman(){
        
        // Pruebas para números arábigos comunes y sus equivalentes romanos
        assertThat(ArabicToRoman.arabicToRoman(1), is("I"));
        assertThat(ArabicToRoman.arabicToRoman(2), is("II"));
        assertThat(ArabicToRoman.arabicToRoman(3), is("III"));
        assertThat(ArabicToRoman.arabicToRoman(5), is("V"));
        assertThat(ArabicToRoman.arabicToRoman(6), is("VI"));
        assertThat(ArabicToRoman.arabicToRoman(7), is("VII"));
        assertThat(ArabicToRoman.arabicToRoman(10), is("X"));
        assertThat(ArabicToRoman.arabicToRoman(11), is("XI"));
        assertThat(ArabicToRoman.arabicToRoman(15), is("XV"));
        assertThat(ArabicToRoman.arabicToRoman(16), is("XVI"));
        assertThat(ArabicToRoman.arabicToRoman(50), is("L"));
        assertThat(ArabicToRoman.arabicToRoman(51), is("LI"));
        assertThat(ArabicToRoman.arabicToRoman(55), is("LV"));
        assertThat(ArabicToRoman.arabicToRoman(56), is("LVI"));
        assertThat(ArabicToRoman.arabicToRoman(60), is("LX"));
        assertThat(ArabicToRoman.arabicToRoman(70), is("LXX"));
        assertThat(ArabicToRoman.arabicToRoman(80), is("LXXX"));
        assertThat(ArabicToRoman.arabicToRoman(81), is("LXXXI"));
        assertThat(ArabicToRoman.arabicToRoman(85), is("LXXXV"));
        assertThat(ArabicToRoman.arabicToRoman(86), is("LXXXVI"));
        assertThat(ArabicToRoman.arabicToRoman(126), is("CXXVI"));
        assertThat(ArabicToRoman.arabicToRoman(2507), is("MMDVII"));
        
        assertThat(ArabicToRoman.arabicToRoman(4), is("IV"));
        assertThat(ArabicToRoman.arabicToRoman(9), is("IX"));
        assertThat(ArabicToRoman.arabicToRoman(14), is("XIV"));
        assertThat(ArabicToRoman.arabicToRoman(19), is("XIX"));
        assertThat(ArabicToRoman.arabicToRoman(24), is("XXIV"));
        assertThat(ArabicToRoman.arabicToRoman(40), is("XL"));
        assertThat(ArabicToRoman.arabicToRoman(49), is("XLIX"));
        assertThat(ArabicToRoman.arabicToRoman(90), is("XC"));
        assertThat(ArabicToRoman.arabicToRoman(99), is("XCIX"));
        assertThat(ArabicToRoman.arabicToRoman(400), is("CD"));
        assertThat(ArabicToRoman.arabicToRoman(900), is("CM"));
        assertThat(ArabicToRoman.arabicToRoman(2984), is("MMCMLXXXIV"));
        
    }
}
