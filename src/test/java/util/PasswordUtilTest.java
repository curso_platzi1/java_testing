
package util;

import junit.framework.Assert;
import static util.PasswordUtil.SecurityLevel.*;
import org.junit.Test;

public class PasswordUtilTest {
    @Test
    public void weak_when_has_less_than_8_letters() {
        Assert.assertEquals(WEAK, PasswordUtil.assessPassword("1234567"));
    }
    
    @Test
    public void medium_when_has_letters_and_numbers(){
        Assert.assertEquals(MEDIUM, PasswordUtil.assessPassword("1234abcd"));
    }
    
    @Test
    public void weak_when_has_only_letters(){
        Assert.assertEquals(WEAK, PasswordUtil.assessPassword("abcdefghi"));
    }
    
    @Test
    public void strong_when_has_letters_number_and_symbols(){
        Assert.assertEquals(STRONG, PasswordUtil.assessPassword("abcd1234!"));
    }
}
