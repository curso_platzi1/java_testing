package util;

import static org.hamcrest.CoreMatchers.*;
import org.junit.Test;
import static org.junit.Assert.*;

public class FizzBuzzTest {
    @Test
    public void fizz_when_is_divisible_by_3(){
        assertThat(FizzBuzz.fizzBuzz(3), is("Fizz"));
        assertThat(FizzBuzz.fizzBuzz(6), is("Fizz"));
    }
    
    @Test
    public void buzz_when_is_divisible_by_5(){
        assertThat(FizzBuzz.fizzBuzz(5),is("Buzz"));
        assertThat(FizzBuzz.fizzBuzz(10),is("Buzz"));
    }
    
    @Test
    public void fizzBuzz_when_is_divisible_by_3_and_5(){
        assertThat(FizzBuzz.fizzBuzz(15),is("FizzBuzz"));
        assertThat(FizzBuzz.fizzBuzz(30),is("FizzBuzz"));
    }
    
    @Test
    public void number_when_is_different_value(){
        assertThat(FizzBuzz.fizzBuzz(2),is("2"));
        assertThat(FizzBuzz.fizzBuzz(16),is("16"));
        
    }
}
