package util;

import org.junit.Assert;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class StringUtilTest {
    @Test
    public void repeat_once() {
        Assert.assertEquals("hola",StringUtil.repeat("hola", 1));
    }
    
    @Test
    public void repeat_zero_times(){
        Assert.assertEquals("", StringUtil.repeat("Hola", 0));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void repeat_negative_times(){
        Assert.assertEquals("", StringUtil.repeat("hola", -1));
    }
    
    @Test
    public void isEmpty_true(){
        assertTrue(StringUtil.isEmpty(""));
    }
    
    @Test
    public void isEmpty_null(){
        assertTrue(StringUtil.isEmpty(null));
    }
    
    @Test
    public void isEmpty_false(){
        assertFalse(StringUtil.isEmpty("         Hola mundo"));
    }
}
