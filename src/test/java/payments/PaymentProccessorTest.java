package payments;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;

public class PaymentProccessorTest {
    
    // Declaración de variables necesarias para las pruebas
    private PaymentGateway paymentGateway;
    private PaymentProccessor paymentProccessor;
    
    // Método anotado con @Before se ejecuta antes de cada prueba
    @Before
    public void setup(){
        
        // Configuramos un objeto simulado (mock) de PaymentGateway y lo asociamos con el PaymentProccessor
        paymentGateway = Mockito.mock(PaymentGateway.class);
        paymentProccessor = new PaymentProccessor(paymentGateway);
    }
    
    // Prueba para verificar si el pago es correcto
    @Test
    public void payment_is_correct() {
        
        // Configuramos el mock para que devuelva un objeto PaymentResponse con estado OK
        Mockito.when(paymentGateway.requestPayment(Mockito.any())).
                thenReturn(new PaymentResponse(PaymentResponse.PaymentStatus.OK));
        
        // Verificamos que el método makePayment devuelve true para un pago exitoso
        assertTrue(paymentProccessor.makePayment(1000));
    }
    
    // Prueba para verificar si el pago es incorrecto
    @Test
    public void payment_is_wrong() {
        
        // Configuramos el mock para que devuelva un objeto PaymentResponse con estado ERROR
        Mockito.when(paymentGateway.requestPayment(Mockito.any())).
                thenReturn(new PaymentResponse(PaymentResponse.PaymentStatus.ERROR));
        
        // Verificamos que el método makePayment devuelve false para un pago con error
        assertFalse(paymentProccessor.makePayment(1000));
    }
}

