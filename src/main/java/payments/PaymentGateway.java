package payments;
// Interfaz que define un método para solicitar pagos a través de una pasarela de pago
public interface PaymentGateway {
    PaymentResponse requestPayment(PaymentRequest request);  
}
