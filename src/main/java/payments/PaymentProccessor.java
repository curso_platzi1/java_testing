package payments;
// Clase que procesa pagos utilizando una pasarela de pago
public class PaymentProccessor {
    
    private PaymentGateway paymentGateway;
    
    // Clase que procesa pagos utilizando una pasarela de pago
    public PaymentProccessor(PaymentGateway paymentGateway) {
        this.paymentGateway = paymentGateway;
    }
    
    // Método para realizar un pago con una cantidad determinada
    public boolean makePayment(double amount){
        
        // Utiliza la pasarela de pago para solicitar un pago con la cantidad proporcionada
        PaymentResponse response = paymentGateway.requestPayment(new PaymentRequest(amount));
        
        // Comprueba si el estado del pago en la respuesta es "OK" y devuelve true si es el caso
        return response.getStatus() == PaymentResponse.PaymentStatus.OK;
    }
    
    // Método para obtener la instancia de PaymentGateway
    public PaymentGateway getPaymentGateway() {
        return paymentGateway;
    }

    // Método para establecer la instancia de PaymentGateway
    public void setPaymentGateway(PaymentGateway paymentGateway) {
        this.paymentGateway = paymentGateway;
    }
    
}
