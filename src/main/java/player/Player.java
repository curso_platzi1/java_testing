package player;

// Clase que representa a un jugador que juega un juego de azar con un dado
public class Player {
    
    private Dice dice; // Dado que el jugador utilizará
    private int minNumberToWin; // Número mínimo necesario para ganar

    // Constructor que recibe un objeto Dice y el número mínimo para ganar
    public Player(Dice dice, int minNumberToWin) {
        this.dice = dice;
        this.minNumberToWin = minNumberToWin;
    }
    
    // Método que simula la jugada del jugador
    public boolean play(){
        // Lanza el dado y obtiene un número aleatorio
        int diceNumber = dice.roll();
        
        // Compara el número obtenido con el número mínimo requerido para ganar
        return diceNumber >= this.minNumberToWin;
    }
    
}
