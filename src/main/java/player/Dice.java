package player;

import java.util.Random;

// Clase que representa un dado con un número variable de lados
class Dice {
    
    private int sides; // Número de lados del dado
    
    // Constructor que recibe la cantidad de lados del dado
    public Dice(int sides){
        this.sides = sides;
    }
    
    
    // Método que simula lanzar el dado y devuelve un valor aleatorio entre 1 y el número de lados
    public int roll(){
        return new Random().nextInt(sides) + 1;
    }
        
}
