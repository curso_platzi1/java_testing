
package util;

public class ArabicToRoman {
    public static String arabicToRoman(int number){
        
        // Comprueba si el número está dentro del rango permitido (1 a 3999)
        if(number <= 0 || number > 3999){
            throw new IllegalArgumentException("El numero debe estar en el rango de 1 a 3999");
        }
        
        // Define dos arrays paralelos para almacenar los valores arábigos y los símbolos romanos correspondientes
        int[] arabicValues = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        String[] romanSymbols = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        
        // Inicializa una cadena vacía para almacenar el resultado de la conversión
        String result = "";
                
        // Itera a través de los valores arábigos y símbolos romanos para realizar la conversión
        for (int i = 0; i < arabicValues.length; i++) {
            // Utiliza un bucle while para agregar símbolos romanos mientras el número sea lo suficientemente grande
            while(number >= arabicValues[i]){
                result += romanSymbols[i];
                number -= arabicValues[i];
            }
        }
        
        // Retorna la cadena resultante que representa el número arábigo en notación romana
        return result;
    }
}
