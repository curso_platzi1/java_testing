package util;

import java.util.HashMap;
import java.util.Map;

public class FizzBuzz {
    
    // FizzBuzz consiste en devolver Fizz si un numero n es divisible entre 3 o Buzz si lo es entre 5
    // En caso que no sea divisible entre 3 o 5 se devuelve en forma de cadena el numero 
    
    public static String fizzBuzz(int n){
        // Este método toma un número entero 'n' y devuelve una cadena según las reglas del juego FizzBuzz.

        // Creamos un HashMap para mapear las cadenas ("Fizz" y "Buzz") a sus valores respectivos (3 y 5).
        HashMap<String, Integer> map = new HashMap<>();
        map.put("Fizz", 3);
        map.put("Buzz", 5);

        // Inicializamos una cadena 'result' para construir la respuesta.
        String result = "";

        // Iteramos a través de las entradas del HashMap.
        for(Map.Entry<String, Integer> entry : map.entrySet()){
            // Verificamos si 'n' es divisible por el valor asociado a la cadena ("Fizz" o "Buzz").
            if(n % entry.getValue() == 0){
                // Si es divisible, agregamos la cadena ("Fizz" o "Buzz") a 'result'.
                result += entry.getKey();
            }
        }

        // Si 'result' sigue siendo una cadena vacía, significa que 'n' no es divisible ni por 3 ni por 5, así que devolvemos 'n' como una cadena.
        if(result.equals("")){
            return Integer.toString(n);
        }

        // Devolvemos la cadena 'result', que puede ser "Fizz", "Buzz", o "FizzBuzz" según las reglas del juego.
        return result;
    }
}

