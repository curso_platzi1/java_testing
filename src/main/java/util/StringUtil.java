package util;

public class StringUtil {
    
    // Método para repetir una cadena 'str' 'times' veces
    public static String repeat(String str, int times) {
        
        // Verificamos si 'times' es un número negativo y lanzamos una excepción en ese caso
        if (times < 0) {
            throw new IllegalArgumentException("Los valores negativos no están permitidos");
        }
        
        String result = ""; // Inicializamos una cadena vacía para construir el resultado
        
        // Utilizamos un bucle 'for' para repetir la cadena 'str' 'times' veces
        for (int i = 0; i < times; i++) {
            result += str;
        }
        
        return result; // Devolvemos la cadena resultante
    }
    
    // Método para verificar si una cadena 'str' está vacía o contiene solo espacios en blanco
    public static boolean isEmpty(String str) {
        if (str != null) {
            
            // Eliminamos los espacios en blanco al principio y al final de 'str'
            String newString = str.trim();
            
            // Comprobamos si la cadena resultante está vacía
            return "".equals(newString);
        } else {
            
            // Si 'str' es nula, consideramos que está vacía
            return true;
        }
    }
}

