package util;

public class PasswordUtil {
    
    // Enumeración que define los niveles de seguridad de las contraseñas
    public enum SecurityLevel {
        WEAK, MEDIUM, STRONG
    }
    
    // Método que evalúa el nivel de seguridad de una contraseña
    public static SecurityLevel assessPassword(String password) {
        
        // Comenzamos asumiendo que la contraseña es fuerte
        SecurityLevel securityLevel = SecurityLevel.STRONG;

        // Comprobamos la longitud de la contraseña
        if (password.length() < 8) {
            
            // Si la contraseña tiene menos de 8 caracteres, se considera débil
            securityLevel = SecurityLevel.WEAK;
        } else if (password.matches("[a-zA-Z]+")) {
            
            // Si la contraseña contiene solo letras (mayúsculas o minúsculas), se considera débil
            securityLevel = SecurityLevel.WEAK;
        } else if (password.matches("[a-zA-Z0-9]+")) {
            
            // Si la contraseña contiene letras y números, se considera de nivel medio
            securityLevel = SecurityLevel.MEDIUM;
        }

        // Devolvemos el nivel de seguridad de la contraseña
        return securityLevel;
    }
}

